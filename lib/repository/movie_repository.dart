import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter_starter_private/model/movie/list_movie_response.dart';
import 'package:flutter_starter_private/model/movie/movie_request.dart';
import 'package:flutter_starter_private/utils/api_url.dart';
import 'package:flutter_starter_private/model/GeneralRequest.dart';
import 'package:flutter_starter_private/model/GeneralResponse.dart';
import 'package:flutter_starter_private/utils/api.dart';
import 'package:flutter_starter_private/utils/future_delayer.dart';

abstract class MovieRepository {
  Future<ListMovieResponse> fetchListMovie();
  Future<MovieData> postMovie(MovieCreateRequest movie);
  Future<MovieData> updateMovie(MovieData movie);
}

class MovieRepositoryImpl implements MovieRepository {
  final httpClient = HttpClient();

  @override
  fetchListMovie() async {
    final query = {
      "api_key": '218b2aba5c561e5587d2fb1ff60d3984',
      "language": 'en-US',
      "page": 1,
    };
    final rawResponse = await httpClient.get(ApiUrl.UPCOMING_MOVIE, query: query);
    if (rawResponse is DioError) {
      print(rawResponse);
      throw rawResponse;
    }
    return ListMovieResponse.fromJson((rawResponse as Response).data);
  }

  @override
  Future<MovieData> updateMovie(MovieData movie) async {
    await FutureDelayer.delayBy1000();
    return movie;
  }

  @override
  Future<MovieData> postMovie(MovieCreateRequest movie) async {
    await FutureDelayer.delayBy1000();
    return MovieData(
      backdropPath: '',
      id: Random().nextInt(20000),
      genreIds: movie.genreIds,
      originalLanguage: '',
      originalTitle: 'originalTitle',
      overview: movie.overview,
      popularity: 0,
      releaseDate: DateTime.now(),
      title: movie.title,
      director: movie.director,
      voteAverage: 0,
      voteCount: 0,
    );
  }
}

import 'dart:io';

abstract class FutureDelayer {
  static delayBy1000() => Future.delayed(const Duration(milliseconds: 1000));
  static delayBy1500() => Future.delayed(const Duration(milliseconds: 1500));
  static delayBy2000() => Future.delayed(const Duration(milliseconds: 2000));
}

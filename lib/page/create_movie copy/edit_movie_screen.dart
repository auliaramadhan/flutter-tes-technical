import 'dart:io';
import 'dart:typed_data';

import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_starter_private/model/movie/list_movie_response.dart';
import 'package:flutter_starter_private/theme/image.dart';
import 'package:flutter_starter_private/utils/api_url.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../components/button/button_primary.dart';
import '../../components/form/dropdown_form_border.dart';
import '../../components/form/text_form_border.dart';
import '../../components/spacing.dart';
import '../../components/utils/icon_dark_circle.dart';
import '../../helper/utils.dart';
import '../../mobx/movie/movie_state.dart';
import '../../model/login_data.dart';
import '../../model/movie/movie_request.dart';
import '../../theme/appfont.dart';
import '../../utils/constant.dart';
import '../../utils/storage_helper.dart';
import '../example/example_page.dart';

class EditMovieScreen extends StatefulWidget {
  final MovieData movie;
  const EditMovieScreen({Key? key, required this.movie}) : super(key: key);

  @override
  State<EditMovieScreen> createState() => _EditMovieScreenState();
}

class _EditMovieScreenState extends State<EditMovieScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController summaryField = TextEditingController();
  final TextEditingController titleField = TextEditingController();
  final TextEditingController directorField = TextEditingController();
  List<int> genreList = [];
  String? image;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  initData() async {
    titleField.text = widget.movie.title;
    summaryField.text = widget.movie.overview;
    directorField.text = widget.movie.director;
    genreList = widget.movie.genreIds;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Form(
            key: _formKey,
            child: ListView(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const ExtraHeight(16),
                Text('Edit Movie', style: AppFont.title22),
                const ExtraHeight(4),
                const Text('Silahkan lengkapi data di bawah ini.', style: AppFont.subtitle),
                const ExtraHeight(16),
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Image.network(
                      (widget.movie.posterPath != null) ? ApiUrl.BASE_IMAGE + widget.movie.posterPath! : AppImage.noImage,
                      height: 500,
                    ),
                  ),
                ),
                const ExtraHeight(16),
                TextFormBorder(
                  label: 'TItle',
                  hintText: 'Masukkan title',
                  controller: titleField,
                  validator: Utils.formValidator('title harus diisi'),
                ),
                const ExtraHeight(8),
                TextFormBorder(
                  label: 'Director',
                  hintText: 'Masukkan nama sutradara',
                  controller: directorField,
                  validator: Utils.formValidator('Sutradara harus diisi'),
                ),
                const ExtraHeight(8),
                TextFormBorder(
                  label: 'Summary',
                  hintText: 'Overal story',
                  minLines: 3,
                  maxLines: 5,
                  controller: summaryField,
                ),
                const ExtraHeight(8),
                DropdownFormBorder<int>(
                  label: 'Genre',
                  hintText: 'Masukkan Genre anda',
                  textSetter: (value) => Constant.genre[value] ?? '',
                  onChange: (value) {
                    setState(() {
                      genreList.remove(value);
                      genreList.add(value!);
                    });
                  },
                  list: Constant.genre.keys.toList(),
                ),
                const ExtraHeight(8),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: genreList
                        .map(
                          (e) => Card(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 16),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(Constant.genre[e]!),
                                  ExtraWidth(),
                                  IconButton(
                                    onPressed: () {
                                      genreList.remove(e);
                                      setState(() {});
                                    },
                                    iconSize: 14,
                                    splashRadius: 20,
                                    padding: EdgeInsets.zero,
                                    icon: Icon(Icons.close),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                ),
                const ExtraHeight(8),
                const ExtraHeight(16),
                ButtonPrimary(
                  text: 'Edit',
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      final success = await context.read<MovieState>().update(widget.movie.copyWith(
                            genreIds: genreList,
                            overview: summaryField.text,
                            title: titleField.text,
                            director: directorField.text,
                          ));
                      if (success) {
                        AutoRouter.of(context).pop();
                      }
                    }else {
                      Utils.showAlert(text: 'Form Harap Diisi');
                    }
                  },
                ),
                const ExtraHeight(),
              ],
            )),
        Observer(
          builder: (context) {
            final movieState = context.watch<MovieState>();
            if (movieState.isLoading) {
              return Center(child: CircularProgressIndicator());
            }
            return SizedBox();
          },
        )
      ],
    );
  }
}

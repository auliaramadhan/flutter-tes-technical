import 'package:flutter/material.dart';
import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_starter_private/mobx/movie/movie_state.dart';
import 'package:flutter_starter_private/model/movie/list_movie_response.dart';
import 'package:flutter_starter_private/page/create_movie%20copy/edit_movie_page.dart';
import 'package:flutter_starter_private/page/create_movie/create_movie_page.dart';
import 'package:flutter_starter_private/page/movie_list/movie_list_page.dart';
import 'package:flutter_starter_private/page/splash.dart';
import 'package:provider/provider.dart';

part './routes.gr.dart';

// flutter packages pub run build_runner build --delete-conflicting-outputs

@MaterialAutoRouter(
  // harus ada screen di namanya akan diganti Route
  replaceInRouteName: 'Page,Route',
  routes: [
    AutoRoute(page: SplashPage, initial: true),
    AutoRoute(
      // path: "/movie",
      // name: "MovieWrapperPage",
      page: MovieWrapperPage, // We replace EmptyRouterPage with our
      // BooksWrapperPage now!
      children: [
        AutoRoute(page: MovieListPage, initial: true),
        AutoRoute(page: CreateMoviePage),
        AutoRoute(page: EditMoviePage),
      ],
    ),
  ],
)
class AppAutoRouteRouter extends _$AppAutoRouteRouter {}

class MovieWrapperPage extends StatelessWidget {
  const MovieWrapperPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (context) => MovieState(),
      child: AutoRouter(), // The AutoRouter() widget used here
      // is required to render sub-routes
    );
  }
}

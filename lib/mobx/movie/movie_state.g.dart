// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_state.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieState on _MovieState, Store {
  late final _$isLoadingAtom =
      Atom(name: '_MovieState.isLoading', context: context);

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  late final _$listMovieAtom =
      Atom(name: '_MovieState.listMovie', context: context);

  @override
  ObservableList<MovieData> get listMovie {
    _$listMovieAtom.reportRead();
    return super.listMovie;
  }

  @override
  set listMovie(ObservableList<MovieData> value) {
    _$listMovieAtom.reportWrite(value, super.listMovie, () {
      super.listMovie = value;
    });
  }

  late final _$postMovieAsyncAction =
      AsyncAction('_MovieState.postMovie', context: context);

  @override
  Future<bool> postMovie(MovieCreateRequest movie) {
    return _$postMovieAsyncAction.run(() => super.postMovie(movie));
  }

  late final _$updateAsyncAction =
      AsyncAction('_MovieState.update', context: context);

  @override
  Future<bool> update(MovieData movie) {
    return _$updateAsyncAction.run(() => super.update(movie));
  }

  late final _$getListMovieAsyncAction =
      AsyncAction('_MovieState.getListMovie', context: context);

  @override
  Future<bool> getListMovie() {
    return _$getListMovieAsyncAction.run(() => super.getListMovie());
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
listMovie: ${listMovie}
    ''';
  }
}

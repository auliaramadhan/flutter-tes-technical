import 'package:flutter/foundation.dart';
import 'package:flutter_starter_private/model/movie/list_movie_response.dart';
import 'package:flutter_starter_private/model/movie/movie_request.dart';
import 'package:flutter_starter_private/repository/movie_repository.dart';
import 'package:mobx/mobx.dart';

part 'movie_state.g.dart';

class MovieState = _MovieState with _$MovieState;

abstract class _MovieState with Store {
  final _repo = MovieRepositoryImpl();

  @observable
  bool isLoading = false;

  @observable
  ObservableList<MovieData> listMovie = ObservableList<MovieData>();

  @action
  Future<bool> postMovie(MovieCreateRequest movie) async {
    isLoading = true;
    try {
      final movieResponse = await _repo.postMovie(movie);
      listMovie.add(movieResponse);
    } catch (e) {
      
    }
    isLoading = false;
    return true;
  }
  @action
  Future<bool> update(MovieData movie) async {
    isLoading = true;
    try {
      final movieResponse = await _repo.updateMovie(movie);
      final index =listMovie.indexWhere((value) => value.id == movie.id );
      listMovie[index] = movieResponse;
    } catch (e) {
      
    }
    isLoading = false;
    return true;
  }

  @action
  Future<bool> getListMovie() async {
    isLoading = true;
    try {
      final list = await _repo.fetchListMovie();
      listMovie.addAll(list.results);
    } catch (e) {
      print(e);
    } finally {
      isLoading = false;
    }
    return true;
  }
}

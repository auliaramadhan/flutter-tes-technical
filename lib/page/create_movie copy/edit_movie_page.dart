import 'package:flutter/material.dart';
import 'package:flutter_starter_private/model/movie/list_movie_response.dart';
import 'edit_movie_screen.dart';

class EditMoviePage extends StatelessWidget {
  final MovieData movie;
  const EditMoviePage({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movie Edit'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: EditMovieScreen(movie : movie),
      ),
    );
  }
}

import 'dart:io';
import 'dart:typed_data';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_starter_private/mobx/movie/movie_state.dart';
import 'package:flutter_starter_private/model/movie/list_movie_response.dart';
import 'package:flutter_starter_private/model/movie/movie_request.dart';
import 'package:flutter_starter_private/theme/image.dart';
import 'package:flutter_starter_private/utils/api_url.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../components/button/button_primary.dart';
import '../../components/form/dropdown_form_border.dart';
import '../../components/form/text_form_border.dart';
import '../../components/spacing.dart';
import '../../components/utils/icon_dark_circle.dart';
import '../../helper/utils.dart';
import '../../model/login_data.dart';
import '../../theme/appfont.dart';
import '../../utils/constant.dart';
import '../../utils/storage_helper.dart';
import '../example/example_page.dart';

class CreateMovieScreen extends StatefulWidget {
  const CreateMovieScreen({Key? key}) : super(key: key);

  @override
  State<CreateMovieScreen> createState() => _CreateMovieScreenState();
}

class _CreateMovieScreenState extends State<CreateMovieScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController summaryField = TextEditingController();
  final TextEditingController titleField = TextEditingController();
  final TextEditingController directorField = TextEditingController();
  List<int> genreList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  initData() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Form(
            key: _formKey,
            child: ListView(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const ExtraHeight(16),
                Text('Create Movie', style: AppFont.title22),
                const ExtraHeight(4),
                const Text('Silahkan lengkapi data di bawah ini.', style: AppFont.subtitle),
                const ExtraHeight(16),
                // tidak sempet bikin image pickernya
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Image.network(AppImage.noImage, height: 500),
                  ),
                ),
                const ExtraHeight(16),
                TextFormBorder(
                  label: 'TItle',
                  hintText: 'Masukkan title',
                  controller: titleField,
                  validator: Utils.formValidator('title harus diisi'),
                ),
                const ExtraHeight(8),
                TextFormBorder(
                  label: 'Director',
                  hintText: 'Masukkan nama sutradara',
                  controller: directorField,
                ),
                const ExtraHeight(8),
                TextFormBorder(
                  label: 'Summary',
                  hintText: 'Overal story',
                  minLines: 3,
                  maxLines: 5,
                  controller: summaryField,
                ),
                const ExtraHeight(8),
                DropdownFormBorder<int>(
                  label: 'Genre',
                  hintText: 'Masukkan Genre anda',
                  textSetter: (value) => Constant.genre[value] ?? '',
                  onChange: (value) {
                    setState(() {
                      genreList.remove(value);
                      genreList.add(value!);
                    });
                  },
                  list: Constant.genre.keys.toList(),
                ),
                const ExtraHeight(8),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: genreList
                        .map(
                          (e) => Card(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 16),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(Constant.genre[e]!),
                                  ExtraWidth(),
                                  IconButton(
                                    onPressed: () {
                                      genreList.remove(e);
                                      setState(() {});
                                    },
                                    iconSize: 14,
                                    splashRadius: 20,
                                    padding: EdgeInsets.zero,
                                    icon: Icon(Icons.close),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                ),
                const ExtraHeight(8),
                const ExtraHeight(16),
                ButtonPrimary(
                  text: 'Create',
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      final success = await context.read<MovieState>().postMovie(MovieCreateRequest(
                            genreIds: genreList,
                            overview: summaryField.text,
                            title: titleField.text,
                            director: directorField.text,
                          ));
                      if (success) {
                        AutoRouter.of(context).pop();
                      }
                    } else {
                      Utils.showAlert(text: 'Form Harap Diisi');
                    }
                  },
                ),
                const ExtraHeight(),
              ],
            )),
        Observer(
          builder: (context) {
            final movieState = context.watch<MovieState>();
            if (movieState.isLoading) {
              return Center(child: CircularProgressIndicator());
            }
            return SizedBox();
          },
        )
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'create_movie_screen.dart';

class CreateMoviePage extends StatelessWidget {
  const CreateMoviePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movie Create'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: const CreateMovieScreen(),
      ),
    );
  }
}

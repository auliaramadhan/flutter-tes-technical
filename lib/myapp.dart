import 'package:flutter/material.dart';
import 'package:flutter_starter_private/helper/global.dart';
import 'package:flutter_starter_private/helper/size_config.dart';
import 'package:flutter_starter_private/page/splash.dart';
import 'package:flutter_starter_private/utils/routes.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _autoRouter = AppAutoRouteRouter();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      scaffoldMessengerKey: scaffoldMessengerKey,
      routerDelegate: _autoRouter.delegate(),
      routeInformationParser: _autoRouter.defaultRouteParser(),
    );
  }
}

import 'package:dio/dio.dart';
import 'package:flutter_starter_private/utils/api_url.dart';
import 'package:flutter_starter_private/model/GeneralRequest.dart';
import 'package:flutter_starter_private/model/GeneralResponse.dart';
import 'package:flutter_starter_private/utils/api.dart';

abstract class ExampleRepository {
  Future<GeneralResponse> fetchRegister(GeneralRequest request);
}

class ExampleRepositoryImpl implements ExampleRepository {
  final httpClient = HttpClient();

  @override
  Future<GeneralResponse> fetchRegister(GeneralRequest request) async {
    final rawResponse = await httpClient.post(ApiUrl.EXAMPLE, request);
    if (rawResponse is DioError){
      throw rawResponse;
    }
    return GeneralResponse.fromJson(rawResponse);
  } 
}
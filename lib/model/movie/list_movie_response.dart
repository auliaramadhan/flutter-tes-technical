// To parse this JSON data, do
//
//     final listMovieResponse = listMovieResponseFromJson(jsonString);

import 'dart:convert';

import 'package:flutter_starter_private/helper/converter.dart';

class ListMovieResponse {
  ListMovieResponse({
    required this.page,
    required this.results,
    required this.totalPages,
    required this.totalResults,
  });

  final int page;
  final List<MovieData> results;
  final int totalPages;
  final int totalResults;

  ListMovieResponse copyWith({
    int? page,
    List<MovieData>? results,
    int? totalPages,
    int? totalResults,
  }) =>
      ListMovieResponse(
        page: page ?? this.page,
        results: results ?? this.results,
        totalPages: totalPages ?? this.totalPages,
        totalResults: totalResults ?? this.totalResults,
      );

  factory ListMovieResponse.fromRawJson(String str) => ListMovieResponse.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ListMovieResponse.fromJson(Map<String, dynamic> json) => ListMovieResponse(
        page: json["page"],
        results: jsonToList<MovieData>(json["results"], MovieData.fromJson, returnEmpty: false),
        totalPages: json["total_pages"],
        totalResults: json["total_results"],
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "total_pages": totalPages,
        "total_results": totalResults,
      };
}

class MovieData {
  MovieData({
    required this.backdropPath,
    required this.id,
    required this.genreIds,
    required this.originalLanguage,
    required this.originalTitle,
    required this.overview,
    required this.popularity,
    this.posterPath,
    this.poster,
    required this.releaseDate,
    required this.title,
    required this.director,
    required this.voteAverage,
    required this.voteCount,
  });

  final String backdropPath;
  final int id;
  final String originalLanguage;
  final List<int> genreIds;
  final String originalTitle;
  final String overview;
  final double popularity;
  final String? posterPath;
  final String? poster;
  final DateTime releaseDate;
  final String title;
  final String director;
  final double voteAverage;
  final int voteCount;

  MovieData copyWith({
    String? backdropPath,
    int? id,
    List<int>? genreIds,
    String? originalLanguage,
    String? originalTitle,
    String? overview,
    double? popularity,
    String? posterPath,
    String? poster,
    DateTime? releaseDate,
    String? title,
    String? director,
    double? voteAverage,
    int? voteCount,
  }) =>
      MovieData(
        backdropPath: backdropPath ?? this.backdropPath,
        id: id ?? this.id,
        genreIds: genreIds ?? this.genreIds,
        originalLanguage: originalLanguage ?? this.originalLanguage,
        originalTitle: originalTitle ?? this.originalTitle,
        overview: overview ?? this.overview,
        popularity: popularity ?? this.popularity,
        posterPath: posterPath ?? this.posterPath,
        poster: poster ?? this.poster,
        releaseDate: releaseDate ?? this.releaseDate,
        title: title ?? this.title,
        director: director ?? this.director,
        voteAverage: voteAverage ?? this.voteAverage,
        voteCount: voteCount ?? this.voteCount,
      );

  factory MovieData.fromRawJson(String str) => MovieData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  // di tmdb harus 2 kali buat ambil director 
  factory MovieData.fromJson(Map<String, dynamic> json) => MovieData(
        backdropPath: json["backdrop_path"],
        id: json["id"],
        // genreIds: json["genre_ids"] as List<int>,
        genreIds: List<int>.from((json["genre_ids"] as List).map((e) => e.toInt())),
        originalLanguage: json["original_language"],
        originalTitle: json["original_title"],
        overview: json["overview"],
        popularity: json["popularity"],
        posterPath: json["poster_path"],
        releaseDate: DateTime.parse(json["release_date"]),
        title: json["title"],
        voteAverage: json["vote_average"].toDouble(),
        voteCount: json["vote_count"].toInt(),
        director: "No Director",
      );

  Map<String, dynamic> toJson() => {
        "backdrop_path": backdropPath,
        "id": id,
        "original_language": originalLanguage,
        "original_title": originalTitle,
        "overview": overview,
        "popularity": popularity,
        "poster_path": posterPath,
        "release_date": "${releaseDate.year.toString().padLeft(4, '0')}-${releaseDate.month.toString().padLeft(2, '0')}-${releaseDate.day.toString().padLeft(2, '0')}",
        "title": title,
        "vote_average": voteAverage,
        "vote_count": voteCount,
      };
}


final movieMock = MovieData.fromJson({
  "adult": false,
  "backdrop_path": "/y5Z0WesTjvn59jP6yo459eUsbli.jpg",
  "genre_ids": [27, 53],
  "id": 663712,
  "original_language": "en",
  "original_title": "Terrifier 2",
  "overview":
      "After being resurrected by a sinister entity, Art the Clown returns to Miles County where he must hunt down and destroy a teenage girl and her younger brother on Halloween night.  As the body count rises, the siblings fight to stay alive while uncovering the true nature of Art's evil intent.",
  "popularity": 7866.782,
  "poster_path": "/b6IRp6Pl2Fsq37r9jFhGoLtaqHm.jpg",
  "release_date": "2022-10-06",
  "title": "Terrifier 2",
  "video": false,
  "vote_average": 7.1,
  "vote_count": 445
});

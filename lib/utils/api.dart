import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_starter_private/utils/api_url.dart';
import 'package:flutter_starter_private/utils/custom_interceptor.dart';
import 'package:flutter_starter_private/utils/storage_helper.dart';

class HttpClient {
  late Dio dio;
  late Dio tokenDio;

  HttpClient() {
    dio = Dio(); // with default Options
    tokenDio = Dio(BaseOptions());

    // Set default configs
    dio.options.baseUrl = ApiUrl.BASE_URL;
    dio.options.connectTimeout = 30000; //15s
    dio.options.receiveTimeout = 30000;
    dio.options.headers[HttpHeaders.contentTypeHeader] = "application/json";

    dio.interceptors.addAll({AppInterceptors(dio)});
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    final options = Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return dio.request<dynamic>(requestOptions.path, data: requestOptions.data, queryParameters: requestOptions.queryParameters, options: options);
  }

  Future<dynamic> post(
    String path,
    body, {
    Map<String, dynamic>? query,
    Options? options,
  }) async {
    try {
      options = options ?? Options(method: 'POST');
      final response = await dio.request(path, data: body, options: options);
      return response.data;
    } catch (e) {
      return e;
    }
  }

  Future<dynamic> get(
    String path, {
    Map<String, dynamic>? query,
    Options? options,
  }) async {
    try {
      final response = await dio.request(path, queryParameters: query, options: Options(method: 'GET'));
      return response;
    } catch (e) {
      return e;
    }
  }
}

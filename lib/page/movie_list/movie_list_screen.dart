import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_starter_private/components/form/text_form_border.dart';
import 'package:flutter_starter_private/components/spacing.dart';
import 'package:flutter_starter_private/helper/size_config.dart';
import 'package:flutter_starter_private/mobx/movie/movie_state.dart';
import 'package:flutter_starter_private/model/movie/list_movie_response.dart';
import 'package:flutter_starter_private/theme/app_color.dart';
import 'package:flutter_starter_private/theme/image.dart';
import 'package:flutter_starter_private/utils/api_url.dart';
import 'package:flutter_starter_private/utils/routes.dart';
import 'package:provider/provider.dart';

import '../../components/button/button_primary.dart';
import '../../theme/appfont.dart';

class MovieListScreen extends StatefulWidget {
  const MovieListScreen({Key? key}) : super(key: key);

  @override
  State<MovieListScreen> createState() => _MovieListScreenState();
}

class _MovieListScreenState extends State<MovieListScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.read<MovieState>().getListMovie();
  }

  @override
  Widget build(BuildContext context) {
    final movieState = context.watch<MovieState>();
    final double itemHeight = 360;
    final double itemWidth = SizeConfig.screenWidth / 2;

    return Observer(
      builder: (context) => Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: GridView.builder(
                  itemBuilder: (context, index) {
                    final movie = movieState.listMovie[index];
                    return InkWell(
                      onTap: () {
                        AutoRouter.of(context).push(EditMovieRoute(movie: movie));
                      },
                      child: Card(
                        elevation: 4,
                        child: Padding(
                          padding: EdgeInsets.all(8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              ClipRRect(
                                child: Image.network(
                                  ((movie.posterPath ?? movie.poster) != null) ? ApiUrl.BASE_IMAGE + (movie.posterPath ?? movie.poster!) : AppImage.noImage,
                                  height: 200,
                                ),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              Text(movie.title, style: AppFont.title16, textAlign: TextAlign.center,),
                              Text(movie.director, style: AppFont.subtitle12),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.star, color: Colors.yellow),
                                  Text(movie.voteAverage.toString(), style: AppFont.subtitle12),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: movieState.listMovie.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 8,
                    crossAxisSpacing: 8,
                    childAspectRatio: itemWidth / itemHeight,
                  ),
                ),
              )
            ],
          ),
          if (movieState.isLoading) const Center(child: CircularProgressIndicator())
        ],
      ),
    );
  }
}

final _listMock = List.filled(3, movieMock);

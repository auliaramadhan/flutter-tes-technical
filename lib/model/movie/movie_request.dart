import 'dart:convert';

class MovieCreateRequest {
  MovieCreateRequest({
    required this.genreIds,
    required this.overview,
    required this.title,
    required this.director,
  });

  final List<int> genreIds;
  final String overview;
  final String title;
  final String director;

  factory MovieCreateRequest.fromRawJson(String str) => MovieCreateRequest.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  // di tmdb harus 2 kali buat ambil director 
  factory MovieCreateRequest.fromJson(Map<String, dynamic> json) => MovieCreateRequest(
        // genreIds: json["genre_ids"] as List<int>,
        genreIds: List<int>.from((json["genre_ids"] as List).map((e) => e.toInt())),
        overview: json["overview"],
        title: json["title"],
        director: "No Director",
      );

  Map<String, dynamic> toJson() => {
        "overview": overview,
        "title": title,
        "director": director,
        "genre_ids": genreIds,
      };
}